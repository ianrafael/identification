import sys
import requests 
import threading
import time
import os
import json

import boto3
import cv2
import uuid
from decouple import config

from classes.PessoasController import PessoasController

AWS_ACCESS_KEY = config('AWS_ACCESS_KEY')
AWS_SECRET_ACCESS_KEY = config('AWS_SECRET_ACCESS_KEY')
REGION_NAME = config('REGION_NAME')
COLLECTION_ID = config('COLLECTION_ID')
LOG_ATIVO=config('LOG_ATIVO')
LOG_PATH=config('LOG_PATH')
IMAGE_NAME = "tmpimg.jpg"
FLAG_QUEUE = 1
PATH_TMP_IMGS = 'tmpimgs/'
API_ADDRESS = config('API_ADDRESS')

class DetectController:

    def __init__(self, camera):
        self.camera = camera
        self.start_aprovation_time = 0
        self.current_name =""
        self.sent_id = -1
        self.clear_names = False
        self.person = {}
        self.too_many_faces_status = False
        self.person_data = []
        pass

    def detectar(self):

        threshold = 98
        maxFaces=50

        pessoas = []
        response = []
        status = True

        client = boto3.client(
            'rekognition',
            aws_access_key_id=AWS_ACCESS_KEY,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
            region_name=REGION_NAME,)

        with os.scandir(PATH_TMP_IMGS) as entries:
            for entry in entries:
                try:                                    
                    imgfile = open(entry.path, 'rb')
                    imgbytes = imgfile.read()
                    imgfile.close()
                            
                    imgobj = {'Bytes': imgbytes}

                    result = client.search_faces_by_image(Image=imgobj, CollectionId=COLLECTION_ID, FaceMatchThreshold=threshold, MaxFaces=maxFaces)
                    
                    if (len(result['FaceMatches']) > 0):
                        externalString = result['FaceMatches'][0]['Face']['ExternalImageId']
                        pos = externalString.find('-')

                        if (pos != -1):
                            idPessoa = externalString[:pos]
                            pessoasController = PessoasController()
                            pessoa = pessoasController.findPessoa(idPessoa)
                            pessoasController.registrarVisita(idPessoa)
                            pessoas.append(pessoa)
                            result['person_id'] = idPessoa
                            result['person_data'] = pessoa
                        else:
                            pessoa = externalString

                    response.append(result)                        

                    if(self.str_to_bool(LOG_ATIVO)):
                        logFullPath = LOG_PATH+"/"+idPessoa+"-"+uuid.uuid4().hex
                        img = cv2.imread(entry.path, cv2.COLOR_BGR2RGB)
                        cv2.imwrite(logFullPath+".jpg", img)

                        with open(logFullPath+".json", 'w') as f:
                            json.dump(result, f)  

                except Exception as e:
                    print(str(e))
                    status = False
                    break
            
            current_persons = pessoas
            #print("Tempo Gasto para detectar -> "+str(time.time() - startTime))
            # Debug
            #print(current_persons)
            
            #print("Tempo para aprovar ponto-> "+str(time.time() - startTime))
            return status, current_persons
    
    def str_to_bool(self, s):
        if s == 'True':
            return True
        elif s == 'False':
            return False
    
    def send_detected_name(self,person):      
        person_data = {
                "action": "Fail_recognition",
                "status": False
            }
        
        if person != None:
            person_data = {
                "action": "approve_recognition",
                "status": True,
                "name": person['nome'],
                "id": person['id'],
                "cpf":person['cpf'],
                "ultimaVisita":person['ultimaVisita']
            }
            return person_data

        return person_data
        