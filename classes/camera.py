import cv2
import uuid
import time
import threading

from classes.DetectController import DetectController

import os
import json
from decouple import config

current_milli_time = lambda: int(round(time.time() * 1000))
TEMPO_TOLERANCIA_ROSTO = 0.5
PATH_TMP_IMGS = 'tmpimgs/'
IMAGE_NAME = "tmpimg.jpg"
NUM_CAMERA=config("NUM_CAMERA")

class VideoCamera():
    
    def __init__(self):
        self.possuiFace = False
        self.oldFaces = []
        self.imagemJpeg = None
        self.image = None
        self.available = False
        self.momentoUltimaFaceDetectada = 2
        self.oldFaces = []
        self.detect = DetectController(self)
        self.ultimaRequisicao = time.time()

    def desenharRosto(self, image, x, y, w, h, blur = False):
        cv2.rectangle(image, (x,y), (x+w,y+h), (255,255,0) ,2)

        if (blur):
            result_image = cv2.blur(image, (7, 7))
            sub_face = image[y:y+h, x:x+w]
            result_image[y:y+sub_face.shape[0], x:x+sub_face.shape[1]] = sub_face
            image = result_image

        return image

    def get_frame(self,image):
        self.image = image
        self.available = True            
        self.analisarFace()
        menssage={ 
            "status": True,
            "message":"sucess"
        }
        tempoAtual = time.time()
        #print(self.finish)
        self.ultimaRequisicao = tempoAtual
        self.capturarFoto()
        status , persons_data = self.detect.detectar()        
        if status:
            menssage["persons_data"] = persons_data
            return persons_data
        menssage={ 
            "status": False,
            "message":"Fail",
            "persons_data": persons_data
        }
        return menssage

    def capturarFoto(self):
        i = 1

        with os.scandir(PATH_TMP_IMGS) as entries:
            for entry in entries:
                if entry.is_file() or entry.is_symlink():
                    os.remove(entry.path)

        for (x,y,w,h) in self.oldFaces:
            roi_color = self.image[y:y + h, x:x + w]
            cv2.imwrite(PATH_TMP_IMGS+str(i)+ '_tmpface.jpg', roi_color)
            i=i+1

        cv2.imwrite(IMAGE_NAME, self.image)

    def analisarFace(self):
        face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml') 
        gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY) 
        faces = face_cascade.detectMultiScale(gray, 1.3, 5) 
        
        # Verifica se alguma face foi detectada
        # Caso não tenha sido, verifica se tem X segundos que nenhuma face eh detectada
        # Caso tenha mais tempo que o tempo de tolerancia, marca como nenhuma face
        # Caso tenha menos tempo que o tempo de tolerancia desenha o ultimo quadrado desenhado
        if(len(faces) > 0):

            self.possuiFace = True
            self.momentoUltimaFaceDetectada = current_milli_time()
            self.oldFaces = faces
     

            for (x,y,w,h) in faces:                 
                self.image = self.desenharRosto(self.image, x, y, w, h)
        else:

            if ((current_milli_time() - self.momentoUltimaFaceDetectada) > TEMPO_TOLERANCIA_ROSTO * 1000):
                self.possuiFace = False    
            else:
                if (len(self.oldFaces) > 0):
                    for (x,y,w,h) in self.oldFaces: 
                        self.image = self.desenharRosto(self.image, x, y, w, h, False)